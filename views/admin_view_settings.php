<div class="wrap">
	<h2 style="text-align:center;padding-bottom:15px;">Weight Loss Calculator Plugin Configuration</h2>
	<form action="options.php" method="post" enctype="multipart/form-data">
	<?php
 		settings_fields('wl_calculator-settings');
	?> 
		<table class="form-table">
			<tr>
				<th scope="row"><label for="wpwl_mc_api_key"><?php _e('Mail Chimp Api Key') ?></label></th>
				<td><input type="text" name="wpwl_mc_api_key" id="wpwl_mc_api_key" placeholder="Mail Chimp Api Key" value="<?php echo get_option('mail_chimp_api_key'); ?>" style="width:100%;" ></td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ft_test_accept_mail_content"><?php _e('Select the list for user subscription') ?></label>
					<p style="padding-right:15px">You can use [freelance_name] for database stored freelance name substitution</p>  
				</th>
				<td>
					<select name="mail_chimp_list" id="mail_chimp_list">
					<?php
						$mail_chimp_list = get_option('subscription_list');
						$lists = $this->ft_admin_get_mailchimp_lists();
						
						foreach($lists as $list) { ?>
						<option value="xxxxx">optin1</option>   	
						<?php
	                     
						}
					?>
					</select>   
				</td>
			</tr>
			
		</table>
		<?php 
		do_settings_sections('freelance_test-settings'); 
		@submit_button();?>
	</form>
</div>