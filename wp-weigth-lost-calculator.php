<?php
/*
Plugin Name: Weigth Lost Calculator
Description: Displays a weigth lost calculator and subscription to mailchimp list.
Version: 1.0.0
Author: Juan Carlos Quevedo Lusson
Author email: vitralsolutions@gmail.com
Author URI: https://wpkraken.io
License: GPL v3

Weigth Lost Calculator
Copyright (C) 2017 WP Kraken

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Prevent direct file access
defined( 'ABSPATH' ) or exit;


include_once('includes/api/class-api-v3.php'); 
include_once('includes/api/class-api-v3-client.php');
include_once('includes/api/class-exception.php');     
include_once('includes/api/class-connection-exception.php'); 
include_once('includes/api/class-resource-not-found-exception.php'); 

if(!class_exists( 'WP_Weigth_Lost_Calculator' ) ) {
 	class WP_Weigth_Lost_Calculator {
		private $mail_chimp_object; 		
 		public function __construct (){
 		    $mail_chimp_api_key = '44c462a4b44863ec46a3e73124f3d40f-us17';
			$this->mail_chimp_object = new MC4WP_API_v3($mail_chimp_api_key);
 			
 			add_action('admin_menu', array($this, 'ft_view_menu'));   
			add_action( 'admin_init', array( $this,'ft_register_settings'));
 		}
 		public function ft_admin_get_mailchimp_lists(){
 			$lists = $this->mail_chimp_object->get_lists();
 			return $lists;
 		}
 		public function ft_admin_view_settings(){
			// Checking if the user has priviledges for managing options else show a warning.
			if ( !current_user_can( 'manage_options' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
			}
			include('views/admin_view_settings.php');
			
		}
 		public function ft_register_settings() {
			register_setting( 'wl_calculator-settings', 'mail_chimp_api_key' );
			register_setting( 'wl_calculator-settings', 'subscription_list');
			register_setting( 'wl_calculator-settings', 'email_for_notifications');
		}
 		
 		public function ft_view_menu() {
			add_options_page( 'Weight Loss Calcutator Configuration', 'Weight Loss Calculator', 'manage_options', 'wl_calculator-settings', array($this,'ft_admin_view_settings') );
		} 
 	}
 	
 	$wpwl_calculator = new WP_Weigth_Lost_Calculator();
}     


